<?php

function zbench_preprocess_page(&$vars) {
  $vars['footer_msg'] = ' &copy; ' . $vars['site_name'] . ' ' . date('Y');
  $vars['search_box'] = str_replace(t('Search this site: '), '', $vars['search_box']);

  $vars['p_links'] = '';
  if(!empty($vars['primary_links'])) {

    foreach ($vars['primary_links'] as $link) {
      $link_current = '';
      $attributes = 'class="page-item"';

      $href_attributes = '';
      $href = url($link['href']);

      if ($link['href'] == '<front>') {
        $attributes = '';
        
        if (drupal_is_front_page())
        $attributes .= ' class="current_page_item"';
      }

      if($link['href'] == $_GET['q']) {
        $attributes = 'class="current_page_item"';
      }

      $vars['p_links'] .= '<li ' . $attributes . '><a ' . $href_attributes . ' href="' . $href . '" >' . $link['title'] . '</a></li>';
    }
  }

  $vars['p_links'] = '<ul>' . $vars['p_links'] . '</ul>';

}

function zbench_preprocess_node(&$vars) {
  $vars['post_day'] = format_date($vars['node']->created, 'custom', 'd');
  $vars['post_month'] = format_date($vars['node']->created, 'custom', 'M');
  $vars['post_year'] = format_date($vars['node']->created, 'custom', 'Y');
  $vars['author'] = theme('username', $vars['node']);
  if ($vars['author'])
  $vars['posted_by'] = t('By') . ' ' . $vars['author'];
}

function zbench_preprocess_comment_wrapper(&$vars) {
  $node = $vars['node'];
  $vars['header'] = t('<strong>!count comments</strong> on %title', array('!count' => $node->comment_count, '%title' => $node->title));
}

function zbench_preprocess_comment(&$vars) {
  $vars['classes'] = array('comment');
  if ($vars['zebra'] == 'even') {
    $vars['classes'][] = 'alt';
  }
  $vars['classes'] = implode(' ', $vars['classes']);
}

function zbench_fix_tags($terms, $separator = ' ') {
  $output = '';
  if (is_array($terms)) {
    $links = array();

    foreach ($terms as $term) {
      $links[] = l($term->name, taxonomy_term_path($term), array('rel' => 'tag', 'title' => strip_tags($term->description)));
    }

    $output .= implode($separator, $links);
  }

  return $output;
}

function zbench_fix_post_links($links, $separator = ' ') {
  $r = array();
  foreach($links as $link) {
    $r[] = '<a href="' . url($link['href']) . (!empty($link['fragment']) ? '#' . $link['fragment'] : '') . '" ' . zbench_link_attributes($link['attributes']) . ' >' . $link['title'] . '</a>';
  }

  $r = implode($separator, $r);
  return $r;
}

function zbench_link_attributes($attributes) {
  $r = ' ';
  if (is_array($attributes)) {
    foreach ($attributes as $key => $value) {
      $r .= $key . '="' . $value . '" ';
    }
  }
  return $r;
}



function zbench_search_theme_form($form) {
  $form['#id'] = 'searchform';
  $form['submit']['#type'] = 'button';
  $form['submit']['#id'] = 'searchsubmit';
  $form['submit']['#attributes']['class'] = '';
  $form['submit']['#value'] = t('');
  $form['search_theme_form']['#attributes']['class'] = '';
  $form['search_theme_form']['#id'] = 's';
  $form['search_theme_form']['#title'] = t('');
  return drupal_render($form);
}

function zbench_feed_icon($url, $title) {
    return '<a href="' . check_url($url) . '" title="' . $title . '" rel="nofollow">' . $title . '</a>';
}


