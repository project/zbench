<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head profile="http://gmpg.org/xfn/11">
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
   </head>
  <body>
    <div id="nav">
	  <div id="menus">
		<?php print $p_links; ?>
      </div>
      <?php if ($search_box): ?>
	    <div id="search">
		  <?php print $search_box ?>
        </div>
      <?php endif; ?>
    </div>
    <div id="wrapper">
	  <div id="header">
	    <h1><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
	    <?php if ($site_slogan): ?><h2><?php print $site_slogan; ?></h2><?php endif; ?>
	    <div id="rss"><?php print $feed_icons; ?></div>
	  </div>
      <hr />
      <div id="content">
        <?php if ($is_front && !empty($mission)): ?>
          <div id="headerbanner">
            <p><strong><?php print $mission; ?></strong></p>
          </div>
        <?php endif; ?>
        <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   	    <?php if (!empty($title) && !$node): ?><h2 ><?php print $title; ?></h2><?php endif; ?>
        <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
   	    <?php if (!empty($messages)): print $messages; endif; ?>
        <?php if (!empty($help)): print $help; endif; ?>
        
        <?php print $content; ?>
      </div>
      <div id="sidebar-border">
        <?php if ($right): ?>
          <div id="sidebar">
            <?php print $right; ?>
          </div>
        <?php endif; ?>
      </div>
    </div><!--wrapper-->
    <div class="fixed"></div>
    <div id="footer">
      <?php print $footer; ?>   
	  <div id="footer-inside">
		<p>
			 <?php print $footer_msg; ?> &nbsp;|&nbsp;<a href="http://zww.me" >zBench</a> theme for drupal by top <a href="http://topdrupalthemes.net/">drupal themes</a>.
		</p>
		<span id="back-to-top">&Delta; <a href="#nav" rel="nofollow" title="<?php print t('Back to top'); ?>"><?php print t('Top'); ?></a></span>
	  </div>
    </div><!--footer-->
    <?php print $closure; ?>
  </body>
</html>